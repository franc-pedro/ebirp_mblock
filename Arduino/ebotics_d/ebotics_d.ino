int PinSpeedMA = 5, PinSpeedMB = 6; // DIGITAL PIN FOR THE SPEED OF THE MOTORS
int PinTurnMA = 4, PinTurnMB = 7; // DIGITAL PIN FOR DIRECTION OF THE MOTORS
int TrigPin = 13;  // ULTRASONIC SENSOR PINS
int EchoPin = 12;  
float SSound = 0.0343;  // SOUND SPEED IN cm/us
long Lengh, Distance ;  // VARIABLES TO CALCULATE THE DISTANCE IN cm
int PinLED1 = 9, PinLED2 = 10; // LED1 AND LED2 DIGITAL PINS

void setup() {
  // put your setup code here, to run once:
  //CONFIGURATION OF THE DIGITAL PORTS
  pinMode(PinSpeedMA, OUTPUT);
  pinMode(PinSpeedMB, OUTPUT);
  pinMode(PinTurnMA, OUTPUT);
  pinMode(PinTurnMB, OUTPUT);
  pinMode(TrigPin, OUTPUT);  
  pinMode(EchoPin, INPUT);
  pinMode(PinLED1, OUTPUT);
  pinMode(PinLED2, OUTPUT); 
  // SPEED OF THE MOTORS  100 TO 255
  analogWrite(PinSpeedMA, 175);
  analogWrite(PinSpeedMB, 175);
}

void loop() {
  // put your main code here, to run repeatedly:
  DistanceCM(); // CALL THE FUNCTION TO CALCULATE THE DISTANCE
  if (( Distance < 25) && ( Distance > 1)) // IF THE DISTANCE IS BETWEEN 1 AND 25CM
    {
      digitalWrite(PinLED1,HIGH);//LED1 Y LED2 = ON
      digitalWrite(PinLED2,HIGH);
      digitalWrite(PinTurnMA, HIGH);// CODE&DRIVE BACKWARD
      digitalWrite(PinTurnMB, LOW);
      delay(1000);
      digitalWrite(PinTurnMA, HIGH);// CODE&DRIVE ROTATION
      digitalWrite(PinTurnMB, HIGH);   
      delay(1000);  
    }
    else
    {
      digitalWrite(PinLED1,LOW);// LED1 Y LED2 = OFF
      digitalWrite(PinLED2,LOW);
      digitalWrite(PinTurnMA, LOW); // CODE&DRIVE FORWARD
      digitalWrite(PinTurnMB, HIGH);      
    }
}

void DistanceCM()// DISTANCE CALCULATION FUNCTION
{
  // CALCULATE THE DISTANCE IN CM
  digitalWrite(TrigPin, LOW);        // VERIFY THAT THE TRIGGER IS DEACTIVATED
  delayMicroseconds(4);              // VERIFY THAT THE TRIGGER IS LOW
  digitalWrite(TrigPin, HIGH);       // ACTIVATE THE OUTPUT PULSE
  delayMicroseconds(14);             // WAIT 10µs. PULSE REMAINS ACTIVE DURING
  digitalWrite(TrigPin, LOW);        // STOP PULSE AND WAIT FOR ECHO
  Lengh = pulseIn(EchoPin, HIGH) ; // pulseIn MEASURES THE TIME THAT TAKES TO THE DECLARED PIN (echoPin) TO CHANGE FROM LOW TO HIGH STATUS (FROM 0 TO 1)
  Distance = SSound* Lengh / 2;  // CALCULATE DISTANCEta
