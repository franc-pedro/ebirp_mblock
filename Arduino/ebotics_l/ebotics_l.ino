int MdirectionA = 4, velocityA = 5, MdirectionB = 7, velocityB = 6; // SPEED AND DIRECTION OF MOTOR A AND MOTOR B

int pinR = 2, pinL = 3, followerR, followerL; // LINE FOLLOWER SENSOR PINS


void setup() {

  //BUILD&CODE 4IN1 PORTS CONFIGURATION

  pinMode ( MdirectionA, OUTPUT);
  pinMode ( velocityA, OUTPUT);
  pinMode ( MdirectionB, OUTPUT);
  pinMode ( velocityB, OUTPUT);
  pinMode (pinR, INPUT);
  pinMode (pinL, INPUT);

}

void loop() {

// LINE FOLLOWER SENSOR READINGS

followerR = digitalRead (pinR);
followerL = digitalRead (pinL);

if ((followerR == 0)& (followerL == 0)) // FORWARD 
{
    //MOTOR B
  analogWrite (velocityB, 150);   // MOTOR B SPEED, FROM 0 (STOP) TO 255 (MAXIMUM SPEED)
digitalWrite ( MdirectionB,LOW); // MOTOR B ROTATION DIRECTION, LOW (FORWARD), HIGH (BACKWARD)

   //MOTOR A

analogWrite (velocityA, 150);   // MOTOR A SPEED, FROM 0 (STOP) TO 255 (MAXIMUM SPEED)
digitalWrite ( MdirectionA,HIGH); // MOTOR A ROTATION DIRECTION, HIGH (FORWARD), LOW (BACKWARD)


}

if ((followerR == 0)& (followerL == 1)) //RIGHT
{
    //MOTOR B
  analogWrite (velocityB, 0);   // MOTOR B SPEED, FROM 0 (STOP) TO 255 (MAXIMUM SPEED)
digitalWrite ( MdirectionB,LOW);  // MOTOR B ROTATION DIRECTION, LOW (FORWARD), HIGH (BACKWARD)

     //MOTOR A

analogWrite (velocityA, 150);   // MOTOR A SPEED, FROM 0 (STOP) TO 255 (MAXIMUM SPEED)
  digitalWrite ( MdirectionA,HIGH); //MOTOR A ROTATION DIRECTION, LOW (FORWARD), HIGH (BACKWARD)

}

if ((followerR == 1)& (followerL == 0)) //LEFT 
{
    //MOTOR B
  analogWrite (velocityB, 150);   // MOTOR B SPEED, FROM 0 (STOP) TO 255 (MAXIMUM SPEED)
  digitalWrite ( MdirectionB,LOW);   // MOTOR B ROTATION DIRECTION, LOW (FORWARD), HIGH (BACKWARD)

     //MOTOR A
  analogWrite (velocityA, 0);    // MOTOR A SPEED, FROM 0 (STOP) TO 255 (MAXIMUM SPEED)
  digitalWrite ( MdirectionA,HIGH); //MOTOR A ROTATION DIRECTION, LOW (FORWARD), HIGH (BACKWARD)

}

if ((followerR == 1)& (followerL == 1)) //BACK
{
    //MOTOR B

  analogWrite (velocityB, 150);  // MOTOR B SPEED, FROM 0 (STOP) TO 255 (MAXIMUM SPEED)
  digitalWrite ( MdirectionB,HIGH);   // MOTOR B ROTATION DIRECTION, LOW (FORWARD), HIGH (BACKWARD)

     //MOTOR A

  analogWrite (velocityA, 150);    // MOTOR A SPEED, FROM 0 (STOP) TO 255 (MAXIMUM SPEED)
  digitalWrite ( MdirectionA,LOW);   //MOTOR A ROTATION DIRECTION, LOW (FORWARD), HIGH (BACKWARD)

}


}